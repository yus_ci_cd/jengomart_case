# Input variable: server port
variable "server_port" {
  description = "The port the server will use for HTTP requests"
  default     = "8080"
}


# Creating a Variable for ami of type map

variable "ec2_ami" {
  default = "ami-0db8e77c005d79e33"
}

variable "ec2_ami2" {
  default = "ami-052ff42ae3be02b6a"
}

# Creating a Variable for region

variable "region1" {
  default = "us-east-2"
}
variable "region2" {
  default = "us-west-2"
}

# Creating a Variable for instance_type
variable "ec2_instance_type" {
  type = string
}



variable "vpc_cidr_block" {}

variable "public_subnet_1_cidr" {}
variable "public_subnet_2_cidr" {}
variable "public_subnet_3_cidr" {}
variable "public_subnet_4_cidr" {}
variable "private_subnet_1_cidr" {}
variable "private_subnet_2_cidr" {}
variable "private_subnet_3_cidr" {}
variable "private_subnet_4_cidr" {}

# variable "eip_association_address" {}

variable "ec2_keypair" {}
variable "ec2_eastkey" {}
