# Configure the AWS provider
provider "aws" {
  region = var.region1
}
provider "aws" {
  alias = "west"
  region = var.region2
}


# Region 1 Resources
resource "aws_vpc" "vpc_module" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = true

  tags = {
    Name = "prod-vpc"
  }
}

# Data source: query the list of availability zones
data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_subnet" "subnet_1_public" {
  cidr_block        = var.public_subnet_1_cidr
  vpc_id            = aws_vpc.vpc_module.id
  availability_zone = "${var.region1}a"
  map_public_ip_on_launch = true

  tags = {
    Name = "Public-subnet-1"
  }
}
resource "aws_subnet" "subnet_2_public" {
  cidr_block = var.public_subnet_2_cidr
  vpc_id     = aws_vpc.vpc_module.id
  availability_zone = "${var.region1}b"

  tags = {
    Name="Public-subnet-2"
  }
}

resource "aws_subnet" "subnet_3_public" {
  cidr_block = var.public_subnet_3_cidr
  vpc_id     = aws_vpc.vpc_module.id
  availability_zone = "${var.region1}c"

  tags = {
    Name="Public-subnet-3"
  }
}


resource "aws_subnet" "subnet_1_private" {
  cidr_block        = var.private_subnet_1_cidr
  vpc_id            = aws_vpc.vpc_module.id
  availability_zone = "${var.region1}a"

  tags = {
    Name = "Private-subnet-1"
  }
}
resource "aws_subnet" "subnet_2_private" {
  cidr_block = var.private_subnet_2_cidr
  vpc_id     = aws_vpc.vpc_module.id
  availability_zone = "${var.region1}b"

  tags = {
    Name="Private-subnet-2"
  }
}

resource "aws_subnet" "subnet_3_private" {
  cidr_block = var.private_subnet_3_cidr
  vpc_id     = aws_vpc.vpc_module.id
  availability_zone = "${var.region1}c"

  tags = {
    Name="Private-subnet-3"
  }
}



resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.vpc_module.id

  tags = {
    Name = "Public-route-table"
  }
}

resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.vpc_module.id

  tags = {
    Name = "Private-route-table"
  }
}

resource "aws_route_table_association" "public_subnet_1_association" {
  route_table_id = aws_route_table.public_route_table.id
  subnet_id      = aws_subnet.subnet_1_public.id
}
resource "aws_route_table_association" "public_subnet_2_association" {
  route_table_id = aws_route_table.public_route_table.id
  subnet_id = aws_subnet.subnet_2_public.id
}

resource "aws_route_table_association" "public_subnet_3_association" {
  route_table_id = aws_route_table.public_route_table.id
  subnet_id = aws_subnet.subnet_3_public.id
}

resource "aws_route_table_association" "private_subnet_1_association" {
  route_table_id = aws_route_table.private_route_table.id
  subnet_id      = aws_subnet.subnet_1_private.id
}

resource "aws_route_table_association" "private_subnet_2_association" {
  route_table_id = aws_route_table.private_route_table.id
  subnet_id = aws_subnet.subnet_2_private.id
}

resource "aws_route_table_association" "private_subnet_3_association" {
  route_table_id = aws_route_table.private_route_table.id
  subnet_id = aws_subnet.subnet_3_private.id
}


resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.vpc_module.id

  tags = {
    Name = "prod-IGW"
  }
}

resource "aws_route" "igw_route" {
  route_table_id         = aws_route_table.public_route_table.id
  gateway_id             = aws_internet_gateway.internet_gateway.id
  destination_cidr_block = "0.0.0.0/0"
}

# resource "aws_instance" "jengomartuseasta_ec2" {
#   # Creates eight identical aws ec2 instances for AZ1
#   count = 8

#   # All eight instances will have the same ami and instance_type - CentOS 7 
#   ami           = var.ec2_ami
#   instance_type = var.ec2_instance_type
#   key_name      = var.ec2_eastkey

#   ebs_block_device {
#     device_name           = "/dev/sda1"
#     volume_size           = 250
#     delete_on_termination = false
#   }
#   security_groups = [aws_security_group.ec2-SG.id]
#   subnet_id       = aws_subnet.subnet_1_private.id
#   tags = {
#     # The count.index allows you to launch a resource 
#     # starting with the distinct index number 0 and corresponding to this instance.
#     Name = "jengomart_ec2a-${count.index}"
#   }
# }


# resource "aws_instance" "jengomartuseastb_ec2" {
#   # Creates eight identical aws ec2 instances for AZ2
#   count = 8

#   # All six instances will have the same ami and instance_type - CentOS 7 
#   ami           = var.ec2_ami
#   instance_type = var.ec2_instance_type
#   key_name      = var.ec2_eastkey
#   ebs_block_device {
#     device_name           = "/dev/sda1"
#     volume_size           = 250
#     delete_on_termination = false
#   }
#   security_groups = [aws_security_group.ec2-SG.id]
#   subnet_id       = aws_subnet.subnet_2_private.id
#   tags = {
#     # The count.index allows you to launch a resource 
#     # starting with the distinct index number 0 and corresponding to this instance.
#     Name = "jengomart_ec2b-${count.index}"
#   }
# }
# resource "aws_instance" "jengomartuseastc_ec2" {
#   # Creates four identical aws ec2 instances for AZ2
#   count = 4

#   # All four instances will have the same ami and instance_type - CentOS 7 
#   ami           = var.ec2_ami
#   instance_type = var.ec2_instance_type
#   key_name      = var.ec2_eastkey
#   ebs_block_device {
#     device_name           = "/dev/sda1"
#     volume_size           = 250
#     delete_on_termination = false
#   }
#   security_groups = [aws_security_group.ec2-SG.id]
#   subnet_id       = aws_subnet.subnet_2_private.id
#   tags = {
#     # The count.index allows you to launch a resource 
#     # starting with the distinct index number 0 and corresponding to this instance.
#     Name = "jengomart_ec2c-${count.index}"
#   }
# }


# Create a Launch Configuration
resource "aws_launch_configuration" "launch_config" {
  image_id        = var.ec2_ami
  instance_type   = var.ec2_instance_type
  security_groups = ["${aws_security_group.ec2-SG.id}"]
  root_block_device {
    volume_size           = 250
    delete_on_termination = true
  }
  lifecycle {
    create_before_destroy = true
  }
}



# Create a Security Group for an EC2 instance
resource "aws_security_group" "ec2-SG" {
  name   = "EC2-Instance-SG"
  vpc_id = aws_vpc.vpc_module.id
  ingress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  lifecycle {
    create_before_destroy = true
  }
}

# Create a Security Group for an ELB
resource "aws_security_group" "elb-SG" {
  name = "ELB-SG"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create an Autoscaling Group
resource "aws_autoscaling_group" "jengo_autoscale" {
  launch_configuration = aws_launch_configuration.launch_config.id
  availability_zones   = ["us-east-2a", "us-east-2b", "us-east-2c"]

  load_balancers    = ["${aws_elb.jengo-elb.name}"]
  health_check_type = "ELB"

  min_size = 4
  max_size = 10

  tag {
    key                 = "Name"
    value               = "jengo_ASG"
    propagate_at_launch = true
  }
}

# Create an ELB
resource "aws_elb" "jengo-elb" {
  name = "jengo-elb"
  cross_zone_load_balancing = true
  availability_zones = ["us-east-2a", "us-east-2b", "us-east-2c"]
  security_groups = [aws_security_group.elb-SG.id]

  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = var.server_port
    instance_protocol = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    interval            = 30
    target              = "HTTP:${var.server_port}/"
  }
}

#Region 2 resources

resource "aws_vpc" "vpc_module2" {
  provider = aws.west
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = true

  tags = {
    Name = "prod-vpc2"
  }
}

# Data source: query the list of availability zones
# data "aws_availability_zones" "available" {
#   state = "available"
# }

resource "aws_subnet" "subnet_1_public2" {
  provider = aws.west
  cidr_block        = var.public_subnet_1_cidr
  vpc_id            = aws_vpc.vpc_module2.id
  availability_zone = "${var.region2}a"
  map_public_ip_on_launch = true

  tags = {
    Name = "Public2-subnet-1"
  }
}
resource "aws_subnet" "subnet_2_public2" {
  provider = aws.west
  cidr_block = var.public_subnet_2_cidr
  vpc_id     = aws_vpc.vpc_module2.id
  availability_zone = "${var.region2}b"

  tags = {
    Name="Public2-subnet-2"
  }
}

resource "aws_subnet" "subnet_3_public2" {
  provider = aws.west
  cidr_block = var.public_subnet_3_cidr
  vpc_id     = aws_vpc.vpc_module2.id
  availability_zone = "${var.region2}c"

  tags = {
    Name="Public2-subnet-3"
  }
}
resource "aws_subnet" "subnet_4_public2" {
  provider = aws.west
  cidr_block = var.public_subnet_4_cidr
  vpc_id     = aws_vpc.vpc_module2.id
  availability_zone = "${var.region2}d"

  tags = {
    Name="Public2-subnet-4"
  }
}

resource "aws_subnet" "subnet_1_private2" {
  provider = aws.west
  cidr_block        = var.private_subnet_1_cidr
  vpc_id            = aws_vpc.vpc_module2.id
  availability_zone = "${var.region2}a"

  tags = {
    Name = "Private2-subnet-1"
  }
}
resource "aws_subnet" "subnet_2_private2" {
  provider = aws.west
  cidr_block = var.private_subnet_2_cidr
  vpc_id     = aws_vpc.vpc_module2.id
  availability_zone = "${var.region2}b"

  tags = {
    Name="Private2-subnet-2"
  }
}

resource "aws_subnet" "subnet_3_private2" {
  provider = aws.west
  cidr_block = var.private_subnet_3_cidr
  vpc_id     = aws_vpc.vpc_module2.id
  availability_zone = "${var.region2}c"

  tags = {
    Name="Private2-subnet-3"
  }
}

resource "aws_subnet" "subnet_4_private2" {
  provider = aws.west
  cidr_block = var.private_subnet_4_cidr
  vpc_id     = aws_vpc.vpc_module2.id
  availability_zone = "${var.region2}d"

  tags = {
    Name="Private2-subnet-4"
  }
}

resource "aws_route_table" "public_route_table2" {
  provider = aws.west
  vpc_id = aws_vpc.vpc_module2.id

  tags = {
    Name = "Public-route-table2"
  }
}

resource "aws_route_table" "private_route_table2" {
  provider = aws.west
  vpc_id = aws_vpc.vpc_module2.id

  tags = {
    Name = "Private-route-table"
  }
}

resource "aws_route_table_association" "public_subnet_1_association2" {
  provider = aws.west
  route_table_id = aws_route_table.public_route_table2.id
  subnet_id      = aws_subnet.subnet_1_public2.id
}
resource "aws_route_table_association" "public_subnet_2_association2" {
  provider = aws.west
  route_table_id = aws_route_table.public_route_table2.id
  subnet_id = aws_subnet.subnet_2_public2.id
}

resource "aws_route_table_association" "public_subnet_3_association2" {
  provider = aws.west
  route_table_id = aws_route_table.public_route_table2.id
  subnet_id = aws_subnet.subnet_3_public2.id
}

resource "aws_route_table_association" "public_subnet_4_association2" {
  provider = aws.west
  route_table_id = aws_route_table.public_route_table2.id
  subnet_id = aws_subnet.subnet_4_public2.id
}

resource "aws_route_table_association" "private_subnet_1_association2" {
  provider = aws.west
  route_table_id = aws_route_table.private_route_table2.id
  subnet_id      = aws_subnet.subnet_1_private2.id
}

resource "aws_route_table_association" "private_subnet_2_association2" {
  provider = aws.west
  route_table_id = aws_route_table.private_route_table2.id
  subnet_id = aws_subnet.subnet_2_private2.id
}

resource "aws_route_table_association" "private_subnet_3_association2" {
  provider = aws.west
  route_table_id = aws_route_table.private_route_table2.id
  subnet_id = aws_subnet.subnet_3_private2.id
}
resource "aws_route_table_association" "private_subnet_4_association2" {
  provider = aws.west
  route_table_id = aws_route_table.private_route_table2.id
  subnet_id = aws_subnet.subnet_4_private2.id
}

resource "aws_internet_gateway" "internet_gateway2" {
  provider = aws.west
  vpc_id = aws_vpc.vpc_module2.id

  tags = {
    Name = "prod-IGW"
  }
}

resource "aws_route" "igw_route2" {
  provider = aws.west
  route_table_id         = aws_route_table.public_route_table2.id
  gateway_id             = aws_internet_gateway.internet_gateway2.id
  destination_cidr_block = "0.0.0.0/0"
}

# resource "aws_instance" "jengomartuswesta_ec2" {
#   provider = aws.west
#   # Creates eight identical aws ec2 instances for AZ1
#   count = 8

#   # All eight instances will have the same ami and instance_type - CentOS 7 
#   ami           = var.ec2_ami2
#   instance_type = var.ec2_instance_type
#   key_name      = var.ec2_keypair
#   ebs_block_device {
#     device_name           = "/dev/sda1"
#     volume_size           = 250
#     delete_on_termination = false
#   }
#   security_groups = [aws_security_group.ec2-SG2.id]
#   subnet_id       = aws_subnet.subnet_1_private2.id
#   tags = {
#     # The count.index allows you to launch a resource 
#     # starting with the distinct index number 0 and corresponding to this instance.
#     Name = "jengomart_ec2a-${count.index}"
#   }
# }
# resource "aws_instance" "jengomartuswestb_ec2" {
#   provider = aws.west
#   # Creates eight identical aws ec2 instances for AZ1
#   count = 8

#   # All eight instances will have the same ami and instance_type - CentOS 7 
#   ami           = var.ec2_ami2
#   instance_type = var.ec2_instance_type
#   key_name      = var.ec2_keypair
#   ebs_block_device {
#     device_name           = "/dev/sda1"
#     volume_size           = 250
#     delete_on_termination = false
#   }
#   security_groups = [aws_security_group.ec2-SG2.id]
#   subnet_id       = aws_subnet.subnet_1_private2.id
#   tags = {
#     # The count.index allows you to launch a resource 
#     # starting with the distinct index number 0 and corresponding to this instance.
#     Name = "jengomart_ec2b-${count.index}"
#   }
# }

# resource "aws_instance" "jengomartuswestc_ec2" {
#   provider = aws.west
#   # Creates eight identical aws ec2 instances for AZ1
#   count = 8

#   # All eight instances will have the same ami and instance_type - CentOS 7 
#   ami           = var.ec2_ami2
#   instance_type = var.ec2_instance_type
#   key_name      = var.ec2_keypair
#   ebs_block_device {
#     device_name           = "/dev/sda1"
#     volume_size           = 250
#     delete_on_termination = false
#   }
#   security_groups = [aws_security_group.ec2-SG2.id]
#   subnet_id       = aws_subnet.subnet_1_private2.id
#   tags = {
#     # The count.index allows you to launch a resource 
#     # starting with the distinct index number 0 and corresponding to this instance.
#     Name = "jengomart_ec2c-${count.index}"
#   }
# }

# resource "aws_instance" "jengomartuswestd_ec2" {
#   provider = aws.west
#   # Creates eight identical aws ec2 instances for AZ1
#   count = 6

#   # All eight instances will have the same ami and instance_type - CentOS 7 
#   ami           = var.ec2_ami2
#   instance_type = var.ec2_instance_type
#   key_name      = var.ec2_keypair
#   ebs_block_device {
#     device_name           = "/dev/sda1"
#     volume_size           = 250
#     delete_on_termination = false
#   }
#   security_groups = [aws_security_group.ec2-SG2.id]
#   subnet_id       = aws_subnet.subnet_1_private2.id
#   tags = {
#     # The count.index allows you to launch a resource 
#     # starting with the distinct index number 0 and corresponding to this instance.
#     Name = "jengomart_ec2d-${count.index}"
#   }
# }


# Create a Launch Configuration
resource "aws_launch_configuration" "launch_config2" {
  provider = aws.west
  image_id        = var.ec2_ami2
  instance_type   = var.ec2_instance_type
  security_groups = ["${aws_security_group.ec2-SG2.id}"]
   root_block_device {
    volume_size           = 250
    delete_on_termination = true
  }
  lifecycle {
    create_before_destroy = true
  }
}

# Create a Security Group for an EC2 instance
resource "aws_security_group" "ec2-SG2" {
  provider = aws.west
  name   = "EC2-Instance-SG2"
  vpc_id = aws_vpc.vpc_module2.id
  ingress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  lifecycle {
    create_before_destroy = true
  }
}

# Create a Security Group for an ELB
resource "aws_security_group" "elb-SG2" {
  provider = aws.west
  name = "ELB-SG2"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create an Autoscaling Group
resource "aws_autoscaling_group" "jengo_autoscale2" {
  provider = aws.west
  launch_configuration = aws_launch_configuration.launch_config2.id
  availability_zones   = ["us-west-2a", "us-west-2b", "us-west-2c", "us-west-2d"]

  load_balancers    = ["${aws_elb.jengo-elb.name}"]
  health_check_type = "ELB"

  min_size = 6
  max_size = 10

  tag {
    key                 = "Name"
    value               = "jengo_ASG"
    propagate_at_launch = true
  }
}

# Create an ELB
resource "aws_elb" "jengo-elb2" {
  name = "jengo-elb2"
  cross_zone_load_balancing = true 
  provider = aws.west
  availability_zones = ["us-west-2a", "us-west-2b", "us-west-2c", "us-west-2d"]
  security_groups = ["${aws_security_group.elb-SG2.id}"]

  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = var.server_port
    instance_protocol = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    interval            = 30
    target              = "HTTP:${var.server_port}/"
  }
}

resource "aws_efs_file_system" "Jengoefseast" {
  creation_token = "myeastefs"
  
  lifecycle_policy {
    transition_to_ia = "AFTER_7_DAYS"
  }
  tags = {
    "Name" = "MyEFSeast"
  }
  
}

resource "aws_efs_file_system" "Jengoefswest" {
  creation_token = "mywestefs"
  
  lifecycle_policy {
    transition_to_ia = "AFTER_7_DAYS"
  }
  tags = {
    "Name" = "MyEFSwest"
  }
  
}

resource "aws_s3_bucket" "jengomart-state-my-bucket" {
  bucket = "jengomart-state-my-bucket"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "s3ssec" {
  bucket = aws_s3_bucket.jengomart-state-my-bucket.bucket

  rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
}

resource "aws_s3_bucket_versioning" "jengo_versioning" {
  bucket = aws_s3_bucket.jengomart-state-my-bucket.id
  versioning_configuration {
    status = "Enabled"
  }
  
}